package com.laeven.api;
import java.awt.Color;
import java.awt.Component;
import java.awt.Rectangle;
import java.util.ArrayList;
import javax.swing.*;
/**
* <h1>L GUI</h1>
* The L GUI program implements an application that
* makes creating java swing GUI's less painful.
* <p>
*
* @author  Laeven
* @version 1.2
* @since   2018-09-06
*/
public class LGUI
{	
	public JFrame frame;
	private JMenuBar bar;
	private ArrayList<JButton> buttons = new ArrayList<JButton>();
	private ArrayList<JLabel> labels = new ArrayList<JLabel>();
	public Object events = new Object();
	
	/**
	 * Creates a new GUI
	 */
	public LGUI()
	{
		frame = new JFrame();
		
		//Setting the frame size
		frame.setSize(100,100);
		frame.setBackground(Color.BLACK);
		
		// Set the start position
		frame.setLocationRelativeTo(null);
		
		// Set default close action
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// Make frame visible
		frame.setResizable(false);
		
		frame.setTitle("Yo this is a frame");
		frame.setLayout(null);
	}
	
	/**
	 * Creates a new GUI with specific parameters
	 * @param width With of the GUI
	 * @param height Height of the GUI
	 * @param bgcolor Background colour of the GUI
	 * @param resize Can the GUI be resized?
	 * @param title Title of the GUI
	 */
	public LGUI(int width,int height,Color bgcolor,boolean resize,String title)
	{
		frame = new JFrame();
		
		//Setting the frame size
		frame.setSize(width,height);
		frame.setBackground(bgcolor);
		
		// Set the start position
		frame.setLocationRelativeTo(null);
		
		// Set default close action
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// Make frame visible
		frame.setResizable(resize);
		
		frame.setTitle(title);
		frame.setLayout(null);
	}
	
	/*
	 * JOptionPane has a lot of ENUMS associated with the type of message you want to output
	 * However, upon further inspection, a lot of these ENUMS have the same values and thus
	 * There is no need for all of them as there are only 5 unique message boxes
	 */
	
	/**
	 * Icons that are displayed with a message to indicate the purpose of the message
	 */
	public enum msg_type
	{
		WARNING,
		PLAIN,
		ERROR,
		INFO,
		QUESTION
	}
	
	/**
	 * Positions that components can be placed relative to another component
	 */
	public enum pos
	{
		ABOVE,
		BELOW,
		LEFT,
		RIGHT
	}
	
	/**
	 * This method will output a message to the display
	 * @param msg The text displayed inside of the message box
	 * @param title The title of the message box
	 * @param type The icon type of the message box (WARNING, PLAIN, ERROR, INFO, QUESTION)
	 * @param lineReturn The number of chars to write the message before returning line (Set to 0 if you don't want line formatting)
	 */
	public void displayMsg(String msg,String title,msg_type type,int lineReturn)
	{
		if(lineReturn > 0)
		{
			msg = formatText(msg,lineReturn);
		}
		
		switch(type)
		{
			case ERROR:			JOptionPane.showMessageDialog(null,msg,title,JOptionPane.ERROR_MESSAGE); break;
			case INFO:			JOptionPane.showMessageDialog(null,msg,title,JOptionPane.INFORMATION_MESSAGE); break;
			case PLAIN:			JOptionPane.showMessageDialog(null,msg,title,JOptionPane.PLAIN_MESSAGE); break;
			case QUESTION:		JOptionPane.showMessageDialog(null,msg,title,JOptionPane.QUESTION_MESSAGE); break;
			case WARNING:		JOptionPane.showMessageDialog(null,msg,title,JOptionPane.WARNING_MESSAGE); break;
		}
	}
	
	/**
	 * This method displays a data entry prompt message
	 * @param msg The message displayed in the promp box
	 * @param title The title of the prompt box
	 * @return The data entered
	 */
	public String promptMsg(String msg,String title)
	{
		String data = JOptionPane.showInputDialog(null,msg,title,JOptionPane.PLAIN_MESSAGE);
		return data;
	}
	
	///////////////////////////////////////////////////////////
	////// Buttons ////////////////////////////////////////////
	///////////////////////////////////////////////////////////
	
	/**
	 * This method creates a button
	 * @param text The text to be displayed on the button
	 * @param x The X cord of the button
	 * @param y The Y cord of the button
	 * @param width The width of the button
	 * @param height The height of the button
	 */
	public void addButton(String text,int x,int y,int width,int height)
	{
		JButton but = new JButton(text);
		LAction al = new LAction(events);
		
		but.addActionListener(al);
		but.setName(text);
		but.setBounds(x,y,width,height);		
		buttons.add(but);
	}
	
	/**
	 * This method creates a blank button
	 */
	public void addButton()
	{
		JButton but = new JButton();
		LAction al = new LAction(events);
		
		but.addActionListener(al);
		buttons.add(but);
	}
	
	/**
	 * Sets the buttons method name
	 * @param instance The instance of the button
	 * @param newMethodName The new name of the method that will be ran when this buttons is pressed
	 */
	public void setButtonMethodName(int instance,String newMethodName)
	{
		JButton but = buttons.get(instance);
		
		but.setActionCommand(newMethodName);
		buttons.set(instance,but);
	}
	
	/**
	 * Sets the button to be enabled or disabled
	 * @param instance The instance of the button
	 * @param enable The buttons state
	 */
	public void setButtonEnabled(int instance,boolean enable)
	{
		JButton but = buttons.get(instance);
		
		but.setEnabled(enable);
		buttons.set(instance,but);
	}
	
	/**
	 * Sets the name of the button
	 * @param instance The instance of the button
	 * @param name The new name of the button
	 */
	public void setButtonName(int instance,String name)
	{
		JButton but = buttons.get(instance);
		
		but.setName(name);
		buttons.set(instance,but);
	}
	
	/**
	 * Sets the buttons display text
	 * @param instance The instance of the button
	 * @param text The text
	 */
	public void setButtonText(int instance,String text)
	{
		JButton but = buttons.get(instance);
		
		but.setText(text);
		buttons.set(instance,but);
	}
	
	/**
	 * Sets the x coordinate of the button
	 * @param instance The instance of the button
	 * @param x the new x coordinate
	 */
	public void setButtonX(int instance,int x)
	{
		JButton but = buttons.get(instance);
		Rectangle rect = new Rectangle(but.getBounds());
		rect.x = x;
		but.setBounds(rect);
		buttons.set(instance,but);
	}
	
	/**
	 * Sets the y coordinate of the button
	 * @param instance The instance of the button
	 * @param y the new y coordinate
	 */
	public void setButtonY(int instance,int y)
	{
		JButton but = buttons.get(instance);
		Rectangle rect = new Rectangle(but.getBounds());
		rect.y = y;
		but.setBounds(rect);
		buttons.set(instance,but);
	}
	
	/**
	 * Sets the width of the button
	 * @param instance The instance of the button
	 * @param width the new width
	 */
	public void setButtonWidth(int instance,int width)
	{
		JButton but = buttons.get(instance);
		Rectangle rect = new Rectangle(but.getBounds());
		rect.width = width;
		but.setBounds(rect);
		buttons.set(instance,but);
	}
	
	/**
	 * Sets the height of the button
	 * @param instance The instance of the button
	 * @param height The height of the button
	 */
	public void setButtonHeight(int instance,int height)
	{
		JButton but = buttons.get(instance);
		Rectangle rect = new Rectangle(but.getBounds());
		rect.height = height;
		but.setBounds(rect);
		buttons.set(instance,but);
	}
	
	/**
	 * Adds a value to the width of the button
	 * @param instance The instance of the button
	 * @param width the new width
	 */
	public void addButtonWidth(int instance,int width)
	{
		JButton but = buttons.get(instance);
		Rectangle rect = new Rectangle(but.getBounds());
		rect.width += width;
		but.setBounds(rect);
		buttons.set(instance,but);
	}
	
	/**
	 * Adds a value to the height of the button
	 * @param instance The instance of the button
	 * @param height The height of the button
	 */
	public void addButtonHeight(int instance,int height)
	{
		JButton but = buttons.get(instance);
		Rectangle rect = new Rectangle(but.getBounds());
		rect.height += height;
		but.setBounds(rect);
		buttons.set(instance,but);
	}
	
	/**
	 * Adds a value to the x coordinate of the button
	 * @param instance The instance of the button
	 * @param x the new x coordinate
	 */
	public void addButtonX(int instance,int x)
	{
		JButton but = buttons.get(instance);
		Rectangle rect = new Rectangle(but.getBounds());
		rect.x += x;
		but.setBounds(rect);
		buttons.set(instance,but);
	}
	
	/**
	 * Adds a value to the y coordinate of the button
	 * @param instance The instance of the button
	 * @param y the new y coordinate
	 */
	public void addButtonY(int instance,int y)
	{
		JButton but = buttons.get(instance);
		Rectangle rect = new Rectangle(but.getBounds());
		rect.y += y;
		but.setBounds(rect);
		buttons.set(instance,but);
	}
	
	/**
	 * Returns the background colour of the button
	 * @param instance The instance of the button
	 * @return The background colour of the button
	 */
	public Color getButtonColour(int instance)
	{
		return buttons.get(instance).getBackground();
	}
	
	/**
	 * Returns the buttons display text
	 * @param instance The instance of the button
	 * @return the buttons display text
	 */
	public String getButtonText(int instance)
	{
		return buttons.get(instance).getText();
	}
	
	/**
	 * Returns the x coordinate of the button
	 * @param instance The instance of the button
	 * @return the x coordinate of the button
	 */
	public int getButtonX(int instance)
	{
		Rectangle rect = new Rectangle(buttons.get(instance).getBounds());
		return rect.x;
	}
	
	/**
	 * Returns the y coordinate of the button
	 * @param instance The instance of the button
	 * @return the y coordinate of the button
	 */
	public int getButtonY(int instance)
	{
		Rectangle rect = new Rectangle(buttons.get(instance).getBounds());
		return rect.y;
	}
	
	/**
	 * Returns the width of the button
	 * @param instance The instance of the button
	 * @return the width of the button
	 */
	public int getButtonWidth(int instance)
	{
		Rectangle rect = new Rectangle(buttons.get(instance).getBounds());
		return rect.width;
	}
	
	/**
	 * Returns the height of the button
	 * @param instance The instance of the button
	 * @return the height of the button
	 */
	public int getButtonHeight(int instance)
	{
		Rectangle rect = new Rectangle(buttons.get(instance).getBounds());
		return rect.height;
	}
	
	/**
	 * Return the button object
	 * @param instance The instance of the button
	 * @return the button object
	 */
	public JButton getButton(int instance)
	{
		return buttons.get(instance);
	}
	
	///////////////////////////////////////////////////////////
	////// Labels /////////////////////////////////////////////
	///////////////////////////////////////////////////////////
	
	/**
	 * This method creates a label
	 * @param text The text to be displayed in the label
	 * @param x The x cord of the label
	 * @param y The y cord of the label
	 * @param width The width of the label
	 * @param height The height of the label
	 */
	public void addLabel(String text,int x,int y,int width,int height)
	{
		JLabel lab = new JLabel(text);
		
		lab.setBounds(x,y,width,height);
		labels.add(lab);
	}
	
	/**
	 * Returns the x coordinate of the label
	 * @param instance The instance of the label
	 * @return the x coordinate of the label
	 */
	public int getLabelX(int instance)
	{
		Rectangle rect = new Rectangle(labels.get(instance).getBounds());
		return rect.x;
	}
	
	/**
	 * Returns the y coordinate of the label
	 * @param instance The instance of the label
	 * @return the y coordinate of the label
	 */
	public int getLabelY(int instance)
	{
		Rectangle rect = new Rectangle(labels.get(instance).getBounds());
		return rect.y;
	}
	
	/**
	 * Returns the width of the label
	 * @param instance The instance of the label
	 * @return the width of the label
	 */
	public int getLabelWidth(int instance)
	{
		Rectangle rect = new Rectangle(labels.get(instance).getBounds());
		return rect.width;
	}
	
	/**
	 * Returns the height of the label
	 * @param instance The instance of the label
	 * @return the height of the label
	 */
	public int getLabelHeight(int instance)
	{
		Rectangle rect = new Rectangle(labels.get(instance).getBounds());
		return rect.height;
	}
	
	///////////////////////////////////////////////////////////
	////// Menus and Items ////////////////////////////////////
	///////////////////////////////////////////////////////////
	
	/**
	 * Initialise and create the bar for menus to be added on
	 */
	public void createMenuBar()
	{
		if(bar == null)
		{
			bar = new JMenuBar();
			frame.setJMenuBar(bar);
		}
		else
		{
			System.out.println("LGUI>> Menu bar does not exist");
		}
	}
	
	/**
	 * Adds a separator to the menu
	 * @param menuName The name of the menu
	 */
	public void addSeperator(String menuName)
	{
		getMenu(menuName).addSeparator();
	}
	
	/**
	 * Adds a menu to the menu bar
	 * @param menuName The name of the menu
	 */
	public void addMenu(String menuName)
	{
		if(bar != null)
		{
			bar.add(new JMenu(menuName));
		}
	}
	
	/**
	 * Removes a menu from the menu bar
	 * @param menuName The name of the menu
	 */
	public void removeMenu(String menuName)
	{
		bar.remove(getMenu(menuName));
	}
	
	/**
	 * Sets the name of the menu
	 * @param menuName The name of the menu
	 * @param newName The new name of the menu
	 */
	public void setMenuName(String menuName,String newName)
	{
		getMenu(menuName).setText(newName);
	}
	
	/**
	 * Sets the enabled state of the menu
	 * @param menuName The name of the menu
	 * @param enabled The state of the menu
	 */
	public void setMenuEnabled(String menuName,boolean enabled)
	{
		getMenu(menuName).setEnabled(enabled);
	}
	
	/**
	 * Adds an item to the menu
	 * @param menuName The name of the menu
	 * @param itemName The name of the menu item
	 */
	public void addMenuItem(String menuName,String itemName)
	{
		getMenu(menuName).add(new JMenuItem(itemName));
		getMenuItem(menuName,itemName).addActionListener(new LAction(events));
	}
	
	/**
	 * Removes an item from a menu
	 * @param menuName The name of the menu
	 * @param itemName The name of the menu item
	 */
	public void removeMenuItem(String menuName,String itemName)
	{
		getMenu(menuName).remove(getMenuItem(menuName,itemName));
	}
	
	/**
	 * Sets the menu items name
	 * @param menuName The name of the menu
	 * @param itemName The name of the menu item
	 * @param newName The new name of the menu item
	 */
	public void setMenuItemName(String menuName,String itemName,String newName)
	{
		getMenuItem(menuName,itemName).setText(newName);
	}
	
	/**
	 * Sets the enabled state of the menu item
	 * @param menuName The name of the menu
	 * @param itemName The name of the menu item
	 * @param enabled The state of the menu item
	 */
	public void setMenuItemEnabled(String menuName,String itemName,boolean enabled)
	{
		getMenuItem(menuName,itemName).setEnabled(enabled);
	}
	
	/**
	 * Gets the menu based on the name of the menu
	 * @param menuName The name of the menu
	 * @return The menu
	 */
	private JMenu getMenu(String menuName)
	{
		if(bar != null)
		{
			for(int i = 0; i < bar.getMenuCount(); i++)
			{
				if(bar.getMenu(i).getText() == menuName)
				{
					return bar.getMenu(i);
				}
			}
		}
		else
		{
			System.out.println("LGUI>> Menu bar does not exist");
		}
		
		return null;
	}
	
	/**
	 * Gets the menu item based on the name of the menu and menu item
	 * @param menuName The name of the menu
	 * @param itemName The name of the menu item
	 * @return The menu item
	 */
	private JMenuItem getMenuItem(String menuName,String itemName)
	{
		if(bar != null)
		{
			for(int i = 0; i < bar.getMenuCount(); i++)
			{
				if(bar.getMenu(i).getText() == menuName)
				{
					for(int j = 0; j < bar.getMenu(i).getItemCount(); j++)
					{
						if(bar.getMenu(i).getItem(j).getText() == itemName)
						{
							return bar.getMenu(i).getItem(j);
						}
					}
				}
			}
		}
		else
		{
			System.out.println("LGUI>> Menu bar does not exist");
		}
		
		return null;
	}
	
	/**
	 * This method will place a GUI component relative to another component
	 * @param comp The component used as a source
	 * @param relativeComp The component being placed relative to the source
	 * @param position The type of relativity (ABOVE, BELOW, LEFT, RIGHT)
	 * @param padding The amount of padding (in pixels) to add
	 */
	public void placeRelative(Object comp,Object relativeComp,pos position,int padding)
	{
		String[] objName = new String[2];
		objName[0] = comp.getClass().getName();
		objName[1] = relativeComp.getClass().getName();
		
		int newPos = 0;
		
		Rectangle rect = new Rectangle(((Component) comp).getBounds());
		Rectangle relaRect = new Rectangle(((Component) relativeComp).getBounds());
		
		switch(position)
		{
			case ABOVE:
			{
				newPos = rect.y - (relaRect.height + padding);
				((Component) relativeComp).setBounds(rect.x,newPos,relaRect.width,relaRect.height);
				break;
			}
			case BELOW:
			{
				newPos = rect.y + rect.height + padding;
				((Component) relativeComp).setBounds(rect.x,newPos,relaRect.width,relaRect.height);
				break;
			}
			case LEFT:
			{
				newPos = rect.x - relaRect.width + padding;
				((Component) relativeComp).setBounds(newPos,rect.y,relaRect.width,relaRect.height);
				break;
			}
			case RIGHT:
			{
				newPos = rect.x + rect.width + padding;
				((Component) relativeComp).setBounds(newPos,rect.y,relaRect.width,relaRect.height);
				break;
			}
		}
	}
	
	/**
	 * This method will add all of the components created to the GUI and render it
	 */
	public void buildGUI()
	{
		for(int i = 0; i < buttons.size(); i++)
		{
			frame.add(buttons.get(i));
		}
		
		for(int i = 0; i < labels.size(); i++)
		{
			frame.add(labels.get(i));
		}
		
		frame.setVisible(true);
	}
	
	/**
	 * This method will take a string and return it formatted to return every n of characters
	 * @param text The string to format
	 * @param lengthOfLine The number of characters before returning line
	 * @return The formatted string
	 */
	private String formatText(String text,int lengthOfLine)
	{
		// Divide the text up into chunks (completed lines) and the remain (text left that does not a whole line)
		int chunk = text.length() / lengthOfLine;
		int remain = text.length() % lengthOfLine;
		
		// Seeks used to take a snippet out of the string
		int pSeek = 0, seek = 0;
		StringBuilder sb = new StringBuilder();
		
		/**
		 * Loop through each chunk, add the chunk length and check for whitespace
		 * keep moving the seek along until a whitespace is found, then append the
		 * string builder with it along with a return line.
		 */
		for(int i = 0; i < chunk; i++)
		{
			seek += lengthOfLine;
			
			while(text.charAt(seek) != ' ')
			{
				seek++;
				
				if(remain > 0)
				{
					remain--;
				}
				else
				{
					chunk--;
					remain = lengthOfLine - 1;
				}
			}
			
			sb.append(text.substring(pSeek,seek));
			sb.append("\n");
			
			// previous seek position updated
			pSeek = seek;
		}
		
		// If there is some remainder text left, add it
		if(remain > 0)
		{
			sb.append(text.substring(pSeek,pSeek + remain));
		}
		
		text = sb.toString();
		
		return text;
	}
	
	/**
	 * Sets the class that will be used to hold all methods called from components action listeners
	 * @param obj The class chosen
	 */
	public void setEventsClass(Object obj)
	{
		events = obj;
	}
}
