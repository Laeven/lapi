package com.laeven.api;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
/**
* <h1>L Compress</h1>
* The L Compress program implements an application that
* allows you to take files and compress them!
* <p>
*
* @author  Laeven
* @version 1.5
* @since   2018-08-25
*/
public class LCompress
{
	String fileLine;
	
	Scanner file;
	
	ArrayList<String> textLine = new ArrayList<String>();
	
	ArrayList<Character> charType = new ArrayList<Character>();
	ArrayList<Integer> charTypeNum = new ArrayList<Integer>();
	
	ArrayList<String> stringType = new ArrayList<String>();
	ArrayList<Integer> stringTypeNum = new ArrayList<Integer>();
	
	public void compress(String fileName,String encoderKeyFileName)
	{
		/*
		 * charTypeNumSorted - Sorted character appearance array
		 * charTypeSorted - Sorted character array
		 * compKey - Compressor Key 
		 * br - Buffer Reader
		 */
		ArrayList<Integer> charTypeNumSorted = new ArrayList<Integer>();
		ArrayList<Character> charTypeSorted = new ArrayList<Character>();
		HashMap<Integer,Character> compKey = new HashMap<>();
		
		load(fileName);
		
		boolean isStart = true;
		int countFrom = 1;
		
		/**
		 * Find which characters are the most used
		 */
		for(int k = 0; k < textLine.size(); k++)
		{		
			char[] line = textLine.get(k).toCharArray();
			boolean charTypeFound = false;
			
			if(isStart)
			{
				charType.add(line[0]);
				charTypeNum.add(1);
				isStart = false;
			}
			
			for(int i = countFrom; i < line.length; i++)
			{
				charTypeFound = false;
				
				letterCheck:
				for(int j = 0; j < charType.size(); j++)
				{
					if(line[i] == charType.get(j))
					{
						charTypeFound = true;
						charTypeNum.set(j,charTypeNum.get(j) + 1);						
						break letterCheck;
					}
				}
				
				if(!charTypeFound)
				{
					charType.add(line[i]);
					charTypeNum.add(1);
				}
			}
			
			countFrom = 0;
		}
		
		for(int i = 0; i < charType.size(); i++)
		{
			System.out.print(charType.get(i));
			System.out.print(" - ");
			System.out.println(charTypeNum.get(i));
		}
		
		System.out.println("");
		System.out.println("");
		System.out.println("");
		
		// Sort
		while(charTypeNum.size() > 0)
		{
			// Get the first value of the array
			int num = 0;
			int pos = 0;
			
			// As long as there are at least 2 or more values in the array...

			// Compare it with every other number in the array, if a bigger one is found set it to the new bigger number
			for(int i = 0; i < charTypeNum.size(); i++)
			{
				if(num <= charTypeNum.get(i))
				{
					num = charTypeNum.get(i);
					pos = i;
				}
			}
			
			// Add the cherry picked number to the new sorted array
			if(charType.get(pos) != ' ')
			{
				charTypeNumSorted.add(num);
				charTypeSorted.add(charType.get(pos));
			}
			
			charTypeNum.remove(pos);
			charType.remove(pos);
		}

		for(int i = 0; i < charTypeSorted.size(); i++)
		{
			System.out.println(charTypeSorted.get(i) + " - " + charTypeNumSorted.get(i));
		}
		
		// Create a new compression Key
		for(int i = 0; i < charTypeSorted.size(); i++)
		{
			compKey.put(i,charTypeSorted.get(i));
			System.out.println(compKey.get(i) + " " + i);
		}
		
		mapAndEncode(fileName,compKey,encoderKeyFileName);
	}
	
	private void mapAndEncode(String fileName,HashMap<Integer,Character> compK,String encoderKey)
	{
		// Open file
		try
		{
			LineNumberReader lineNum = new LineNumberReader(new FileReader(fileName));
			BufferedWriter bw = new BufferedWriter(new FileWriter(fileName + "-compressed"));
			LEncoder enco = new LEncoder();
			
			try
			{
				StringBuilder sb1 = new StringBuilder();	// Raw Letters
				StringBuilder sb2 = new StringBuilder();	// Letters mapped to numbers using key
				StringBuilder sb3 = new StringBuilder();	// Mapped numbers encoded into smaller letters
				
				for(String line = null; (line = lineNum.readLine()) != null;)
				{
					sb1.setLength(0);
					sb1.append(line);
					
					// Map letters to numbers on the key
					for(int i = 0; i < sb1.length(); i++)
					{
						nextChar:
						for(int j = 0; j < compK.size(); j++)
						{
							System.out.println("-> " + sb1.charAt(i) + " <-" + compK.get(j) + " " + i);
							if(sb1.charAt(i) == ' ')
							{
								sb2.append(" ");
								break nextChar;
							}
							else if(sb1.charAt(i) == compK.get(j))
							{
								if(j > 9)
								{
									sb2.append(",");
									sb2.append(j);
									//sb2.append(",");
								}
								else
								{
									sb2.append(j);
								}
								
								break nextChar;
							}
						}
					}
					
					// Encode each of the numbers using encoder
					StringBuilder tempWord = new StringBuilder();
					
					enco.loadKey(encoderKey);
					
					for(int i = 0; i < sb2.length(); i++)
					{						
						// A comma signals the number is a multi-digit number
						System.out.println(sb2.charAt(i));
						
						/*
						if(tempWord.length() < 18)
						{
							tempWord.append(sb2.charAt(i));
						}
						else
						{
							sb3.append(enco.encode64(Long.parseLong(tempWord.toString())));
							sb3.append(" ");
							tempWord.setLength(0);
							
							if((sb2.length() - i) < 18)
							{
								sb2.replace(0,i,"");
								sb3.append(enco.encode64(Long.parseLong(sb2.toString())));
								sb3.append(" ");
								break chopAndEncode;
							}
						}
						*/
						
						
						if(sb2.charAt(i) == ',')
						{
							if(tempWord.length() > 0)
							{
								sb3.append(enco.encode64(Long.parseLong(tempWord.toString())));
								tempWord.setLength(0);
							}
							
							StringBuilder num = new StringBuilder();
							
							num.append(sb2.charAt(i + 1));
							num.append(sb2.charAt(i + 2));
							
							sb3.append(",");
							sb3.append(enco.encode64(Long.parseLong(num.toString())));
							//sb3.append(",");
							
							i+= 3;
						}
						// If a space is found encode the collected numbers from the word
						else if(sb2.charAt(i) == ' ')
						{
							if(tempWord.length() > 0)
							{
								System.out.println(tempWord.toString());
								sb3.append(enco.encode64(Long.parseLong(tempWord.toString())));
								sb3.append(" ");
								tempWord.setLength(0);
							}
						}
						// Add the next number from the word into the temporary string builder
						else
						{
							tempWord.append(sb2.charAt(i));
						}
					}					
					
					sb3.append(System.lineSeparator());
					bw.write(sb3.toString());
				}
			}
			finally
			{
				lineNum.close();
				bw.close();
			}
			
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * This method takes a file and outputs a new file with every unique word and the number of times they appear
	 * @param fileToGet The file to take as an input
	 * @param fileNameToSaveAs The name of the file as the output
	 * @param printInConsole Prints the result in the console or output as textfile
	 */
	public void stringAppearance(String fileToGet,String fileNameToSaveAs,boolean printInConsole)
	{
		if(fileToGet != fileNameToSaveAs)
		{		
			load(fileToGet);
	
			boolean isStart = true;
			int countFrom = 1;
			
			for(int k = 0; k < textLine.size(); k++)
			{		
				String[] line = textLine.get(k).split(" ");
				boolean stringTypeFound = false;
				
				if(isStart)
				{
					stringType.add(line[0]);
					stringTypeNum.add(1);
					isStart = false;
				}
				
				for(int i = countFrom; i < line.length; i++)
				{
					stringTypeFound = false;
					
					letterCheck:
					for(int j = 0; j < stringType.size(); j++)
					{
						if(line[i].equals(stringType.get(j)))
						{
							stringTypeFound = true;
							stringTypeNum.set(j,stringTypeNum.get(j) + 1);
							
							
							break letterCheck;
						}
					}
					
					if(!stringTypeFound)
					{
						stringType.add(line[i]);
						stringTypeNum.add(1);
					}
				}
				
				countFrom = 0;
			}
			
			if(printInConsole)
			{
				for(int i = 0; i < stringType.size(); i++)
				{
					System.out.print(stringType.get(i));
					System.out.print(" - ");
					System.out.print(stringTypeNum.get(i));
					System.out.print(" - ");
					System.out.println(i);
				}
			}
			else
			{
				PrintWriter writer = null;
				
				try
				{
					writer = new PrintWriter(fileNameToSaveAs, "UTF-8");
					
					for(int i = 0; i < stringType.size(); i++)
					{
						writer.print(stringType.get(i));
						writer.print(" - ");
						writer.println(stringTypeNum.get(i));
					}
				}
				catch (FileNotFoundException e)
				{
					System.out.println("LCompress>> Error! File not found!");
					e.printStackTrace();
				}
				catch (UnsupportedEncodingException e)
				{
					System.out.println("LCompress>> Error! Unsupported encoding!");
					e.printStackTrace();
				}
				
				writer.close();
			}
		}
		else
		{
			System.out.println("LCompress>> Error! Input and Output file names cannot be the same!");
		}
	}
	
	/**
	 * This method takes a file and outputs a new file with every unique letter and the number of times they appear
	 * @param fileToGet The file to take as an input
	 * @param fileNameToSaveAs The name of the file as the output
	 * @param printInConsole Prints the result in the console or output as textfile
	 */
	public void charAppearance(String fileToGet,String fileNameToSaveAs,boolean printInConsole)
	{
		if(fileToGet != fileNameToSaveAs)
		{
			load(fileToGet);
			
			boolean isStart = true;
			int countFrom = 1;
			
			for(int k = 0; k < textLine.size(); k++)
			{		
				char[] line = textLine.get(k).toCharArray();
				boolean charTypeFound = false;
				
				if(isStart)
				{
					charType.add(line[0]);
					charTypeNum.add(1);
					isStart = false;
				}
				
				for(int i = countFrom; i < line.length; i++)
				{
					charTypeFound = false;
					
					letterCheck:
					for(int j = 0; j < charType.size(); j++)
					{
						if(line[i] == charType.get(j))
						{
							charTypeFound = true;
							charTypeNum.set(j,charTypeNum.get(j) + 1);
							
							
							break letterCheck;
						}
					}
					
					if(!charTypeFound)
					{
						charType.add(line[i]);
						charTypeNum.add(1);
					}
				}
				
				countFrom = 0;
			}
			
			if(printInConsole)
			{
				for(int i = 0; i < charType.size(); i++)
				{
					System.out.print(charType.get(i));
					System.out.print(" - ");
					System.out.print(charTypeNum.get(i));
					System.out.print(" - ");
					System.out.println(i);
				}
			}
			else
			{
				PrintWriter writer = null;
				
				try
				{
					writer = new PrintWriter(fileNameToSaveAs, "UTF-8");
					
					for(int i = 0; i < charType.size(); i++)
					{
						writer.print(charType.get(i));
						writer.print(" - ");
						writer.println(charTypeNum.get(i));
					}
				}
				catch (FileNotFoundException e)
				{
					System.out.println("LCompress>> Error! File not found!");
					e.printStackTrace();
				}
				catch (UnsupportedEncodingException e)
				{
					System.out.println("LCompress>> Error! Unsupported encoding!");
					e.printStackTrace();
				}
				
				writer.close();
			}
		}
		else
		{
			System.out.println("LCompress>> Error! Input and Output file names cannot be the same!");
		}
	}
	
	/**
	 * This method loads a file
	 * @param fileName The name of the file
	 */
	private void load(String fileName)
	{
		try
		{
			file = new Scanner(new File(fileName));
			
			while(file.hasNextLine())
			{
				fileLine = file.nextLine();
				textLine.add(fileLine);
			}
		}
		catch (FileNotFoundException e)
		{
			System.out.println("LCompress>> Error! File not found!");
			e.printStackTrace();
		}
	}
}
