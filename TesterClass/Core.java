package com.laeven.api;

import java.awt.Color;
import java.awt.Rectangle;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.BitSet;

import javax.swing.JOptionPane;

import com.laeven.api.LGUI.msg_type;
import com.laeven.api.LGUI.pos;

public class Core
{
	/**
	 * ====================================================================
	 * ATTENTION!!!
	 * ====================================================================
	 * This class is just for testing and is not part of the API!
	 */
	
	LBasic basic = new LBasic();
	LCSV csv = new LCSV();
	LEncoder enco = new LEncoder();
	LCompress com = new LCompress();
	LGUI gui = new LGUI(500,500,Color.BLACK,true,"This is your title");
	
	public static void main(String args[])
	{
		new Core().tester9();
	}
	
	public void tester9()
	{
		gui.setEventsClass(this);
		
		gui.addButton("L",5,5,50,30);
		
		gui.buildGUI();
		//gui.buildGUI();
	}
	
	public void tester8()
	{
		/*
		gui.displayMsg("This is a very dangerous path you do not really want to"
				+ " go on this kind of thing otherwise something very very bad"
				+ " will happen","This is muh title",msg_type.ERROR,50);
				*/

		//System.out.println();
		//System.out.println(getClassName());
		gui.setEventsClass(this);
		
		gui.addButton("Button1",5,5,100,20);
		gui.addButton("Button2",5,5,100,20);
		gui.addButton("Button3",5,5,100,20);
		gui.addButton("Button4",5,5,100,20);
		gui.addButton("Button5",5,5,100,20);
		gui.addButton("Button6",5,5,100,20);
		gui.addButton("Button7",5,5,100,20);
		gui.placeRelative(gui.getButton(0),gui.getButton(1),pos.RIGHT,5);
		gui.placeRelative(gui.getButton(1),gui.getButton(2),pos.BELOW,5);
		gui.placeRelative(gui.getButton(2),gui.getButton(3),pos.RIGHT,5);
		gui.placeRelative(gui.getButton(3),gui.getButton(4),pos.RIGHT,5);
		gui.placeRelative(gui.getButton(4),gui.getButton(5),pos.ABOVE,5);
		
		gui.createMenuBar();
		gui.addMenu("Menu1");
		gui.addMenu("Menu2");
		gui.addMenu("Menu3");
		gui.setMenuEnabled("Menu3",false);
		gui.addMenuItem("Menu1","Code");
		
		gui.buildGUI();
	}
	
	public void L()
	{
		
		gui.setButtonWidth(0,gui.getButtonWidth(0) + 10);
		gui.setButtonHeight(0,gui.getButtonHeight(0) + 10);
	}
	
	
	public void Button1()
	{
		System.out.println("Hello there1");
	}
	public void Button2()
	{
		System.out.println("Hello there2");
	}
	public void Button3()
	{
		System.out.println("Hello there3");
	}
	public void Button4()
	{
		System.out.println("Hello there4");
	}
	public void Button5()
	{
		System.out.println("Hello there5");
	}
	
	public void Code()
	{
		System.out.println("This is cody");
	}
	
	
	public void tester7()
	{		
		byte b = 0;
		byte b1 = 0;
		byte[] b2;
		
		b = basic.setBits(b,"00110110");
		b1 = basic.setBits(b1,"11111111");
		
		System.out.println(b);
		System.out.println(b1);
		
		/*FileOutputStream fos = null;
		try
		{
			fos = new FileOutputStream("yellow.txt");
			fos.write(b);
			fos.write(b1);
			fos.close();
		} catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		String line;
		BufferedReader br;
		try
		{
			br = new BufferedReader(new FileReader("yellow.txt"));
			
			try
			{

				line = br.readLine();
				System.out.println(line);
				b2 = new byte[line.length()];
				b2 = line.getBytes();
				
				for(int i = 0; i < b2.length; i++)
				{
					System.out.println(b2[i]);
				}
				
				System.out.println(basic.getBit(b2[0],7));
			}
			finally
			{
				br.close();
			}
			
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}		
	}
	
	public void tester6()
	{
		com.compress("text.dat","key.dat");
		
		//String n = "11";
		
		//System.out.println(basic.isNumeric(n));
		
		//enco.loadKey("key.dat");
		
		//System.out.println(enco.encode64(999999999999999999L));
		//com.charAppearance("text.dat","texttt.dat",true);
		//System.out.println(enco.decode("g"));
		
		/*
		for(int i = 0; i < enco.symbKey.size(); i++)
		{
			System.out.println(enco.symbKey.get(i));
		}
		
		System.out.println(enco.symbKey.size());
		*/
	}
	
	public void tester5()
	{
		enco.loadKey("key.dat");
		ArrayList<String> textLine = new ArrayList<String>();
		
		for(int i = 0; i < 1000; i++)
		{
			String s = enco.encode64(i);
			long q = enco.decode64(s);
			
			System.out.println(s + " = " + q);
			textLine.add(enco.encode64(i) + " = " + enco.decode64(enco.encode64(i)));
		}
		
		csv.save("textt.d",textLine);
	}
	
	public void tester4()
	{
		enco.generateKey("key.dat",false);
		//enco.generateKeyCustom("chars.dat","key.dat",false);
	}
	
	public void tester3()
	{
		enco.loadKey("key.dat");
		
		/*
		System.out.println(enco.encode64(Long.MAX_VALUE));
		System.out.println(enco.encode64(Long.MAX_VALUE));
		System.out.println(enco.decode64("blPTsbvaOo"));
		System.out.println(enco.symbKey.size());
		*/
		
		for(int i = 0; i < 1000000; i++)
		{
			System.out.println(enco.encode64(i) + " = " + enco.decode64(enco.encode64(i)));
		}
	}
	
	public void tester2()
	{
		enco.loadKey("key.dat");
		System.out.println(enco.encode(123456784));
		System.out.println(enco.decode("Adventure"));
	}
	
	public void tester()
	{
		System.out.println(basic.range(5, 10));
		//System.out.println();
		String ddd = csv.packN("yellow","blue","pink","green");
		csv.saveN("dashy.dat",ddd,ddd,ddd,ddd,ddd,ddd,ddd,ddd,ddd,ddd,ddd,ddd,ddd,ddd,ddd,ddd,ddd,ddd);
		
		csv.load("dashy.dat");
		System.out.println(csv.getAttFromLine(0,1));
		
		enco.generateKey("key.dat",false);
		enco.loadKey("key.dat");
		String neee = enco.encode(100000);
		System.out.println(neee);
		
		//com.stringApp("text.dat","texty.dat",true);
		com.charAppearance("text2.dat","textc.dat",true);
	}
}
