package com.laeven.api;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Scanner;
/**
* <h1>L CSV</h1>
* The L CSV program implements an application that
* allows you to load and save CSV files
* <p>
*
* @author  Laeven
* @version 1.0
* @since   2018-08-25
*/
public class LCSV
{
	public static String fileLine;
	public static Scanner file;
	public static ArrayList<String> textLine = new ArrayList<String>();
	
	/**
	 * This method takes n of attributes and returns a string with them all packed
	 * @param attData Arraylist of attributes to be packed
	 * @return A packed string
	 */
	public String pack(ArrayList<String> attData)
	{
		String packedData = "";
		
		for(int i = 0; i < attData.size() - 1; i++)
		{
			packedData = packedData + attData.get(i) + ",";
		}
		
		packedData = packedData + attData.get(attData.size() - 1);
		
		return packedData;
	}
	
	/**
	 * This method takes n of attributes and returns a string with them all packed
	 * @param attData Array of attributes to be packed
	 * @return A packed string
	 */
	public String pack(String[] attData)
	{
		String packedData = "";
		
		for(int i = 0; i < attData.length - 1; i++)
		{
			packedData = packedData + attData[i] + ",";
		}
		
		packedData = packedData + attData[attData.length - 1];
		
		return packedData;
	}
	
	/**
	 * This method takes n of attributes and returns a string with them all packed
	 * @param att An attribute
	 * @return A packed string
	 */
	public String packN(String... att)
	{
		String packedData = "";
		
		for(int i = 0; i < att.length - 1; i++)
		{
			packedData = packedData + att[i] + ",";
		}
		
		packedData = packedData + att[att.length - 1];
		
		return packedData;
	}
	
	/**
	 * This method saves a CSV file for Arraylists
	 * @param fileName The name of the file
	 * @param data An array list of all the lines to be saved
	 */
	public void save(String fileName,ArrayList<String> data)
	{				
		PrintWriter writer = null;
		
		try
		{
			writer = new PrintWriter(fileName, "UTF-8");
			
			for(int i = 0; i < data.size(); i++)
			{
				writer.println(data.get(i));
			}			
		}
		catch (FileNotFoundException e)
		{
			System.out.println("LCSV>> Error! File not found!");
			e.printStackTrace();
		}
		catch (UnsupportedEncodingException e)
		{
			System.out.println("LCSV>> Error! Unsupported encoding!");
			e.printStackTrace();
		}
		
		writer.close();
	}
	
	/**
	 * This method saves a CSV file for Arrays
	 * @param fileName The name of the file
	 * @param data An array of all the lines to be saved
	 */
	public void save(String fileName,String[] data)
	{				
		PrintWriter writer = null;
		
		try
		{
			writer = new PrintWriter(fileName, "UTF-8");
			
			for(int i = 0; i < data.length; i++)
			{
				writer.println(data[i]);
			}			
		}
		catch (FileNotFoundException e)
		{
			System.out.println("LCSV>> Error! File not found!");
			e.printStackTrace();
		}
		catch (UnsupportedEncodingException e)
		{
			System.out.println("LCSV>> Error! Unsupported encoding!");
			e.printStackTrace();
		}
		
		writer.close();
	}
	
	/**
	 * This method saves a CSV file for Strings
	 * @param fileName The name of the file
	 * @param data The string of data to be saved
	 */
	public void saveN(String fileName,String... data)
	{				
		PrintWriter writer = null;
		
		try
		{
			writer = new PrintWriter(fileName, "UTF-8");
			
			for(int i = 0; i < data.length; i++)
			{
				writer.println(data[i]);
			}			
		}
		catch (FileNotFoundException e)
		{
			System.out.println("LCSV>> Error! File not found!");
			e.printStackTrace();
		}
		catch (UnsupportedEncodingException e)
		{
			System.out.println("LCSV>> Error! Unsupported encoding!");
			e.printStackTrace();
		}
		
		writer.close();
	}
	
	/**
	 * This method loads a specific number of lines from a CSV file
	 * @param fileName The name of the file
	 * @param numOfLines the number of lines to read. Set to 0 to read all possible lines
	 */
	public void load(String fileName,int numOfLines)
	{
		try
		{
			file = new Scanner(new File(fileName));
			
			while(file.hasNextLine() && numOfLines > 0)
			{
				fileLine = file.nextLine();
				textLine.add(fileLine);
				numOfLines --;
			}
		}
		catch (FileNotFoundException e)
		{
			System.out.println("LCSV>> Error! File not found!");
			e.printStackTrace();
		}
	}
	
	/**
	 * This method loads a CSV file
	 * @param fileName The name of the file
	 */
	public void load(String fileName)
	{
		try
		{
			file = new Scanner(new File(fileName));
			
			while(file.hasNextLine())
			{
				fileLine = file.nextLine();
				textLine.add(fileLine);
			}
		}
		catch (FileNotFoundException e)
		{
			System.out.println("LCSV>> Error! File not found!");
			e.printStackTrace();
		}
	}
	
	/**
	 * This method gets the raw line from the next file
	 * @param lineNumber The line number in the file to get from the array
	 * @return The raw line 
	 */
	public String getLine(int lineNumber)
	{
		String s = "";
		
		try
		{
			s = textLine.get(lineNumber);
		}
		catch(ArrayIndexOutOfBoundsException e)
		{
			System.out.println("LCSV>> Error! Line does not exist!");
			e.printStackTrace();
		}
		
		return s;
	}
	
	/**
	 * This method gets an attribute from a specific line
	 * @param lineNumber The line number
	 * @param attributeNum The attribute to get from that line
	 * @return The attribute from that line
	 */
	public String getAttFromLine(int lineNumber,int attributeNum)
	{
		String[] rawLine;
		String att = "";
		
		try
		{
			rawLine = textLine.get(lineNumber).split(",");
			att = rawLine[attributeNum];
		}
		catch(ArrayIndexOutOfBoundsException e)
		{
			System.out.println("LCSV>> Error! Line does not exist!");
			e.printStackTrace();
		}
		
		return att;
	}
}
