package com.laeven.api;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class LAction extends LGUI implements ActionListener
{
	private Object obj = new Object();
	private Method method;
	
	/**
	 * Tells the action listener which class to reflect to
	 * @param o The class to call action methods from
	 */
	public LAction(Object o)
	{
		obj = o;
	}
	
	@Override
	public void actionPerformed(ActionEvent e)
	{
		try
		{
			method = obj.getClass().getMethod(e.getActionCommand());
		} 
		catch (NoSuchMethodException | SecurityException e1)
		{
			System.out.println("LAction>> Error! Method does not exist!");
			e1.printStackTrace();
		}
		
		if(method != null)
		{
			try
			{
				method.invoke(obj);
			} 
			catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e1)
			{
				System.out.println("LAction>> Error! Illegal arguments!");
				e1.printStackTrace();
			}
		}
		
	}
}
