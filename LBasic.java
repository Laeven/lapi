package com.laeven.api;

import java.util.ArrayList;
import java.util.Random;
/**
* <h1>L Basic</h1>
* The L Basic program implements an application that
* has a range of useful and simple functions.
* <p>
*
* @author  Laeven
* @version 1.1
* @since   2018-08-25
*/
public class LBasic
{	
	/**
	 * Return a random number BETWEEN 2 Integers NOT including the 2 Integers
	 * @param low The lowest possible number to choose from (not including this number)
	 * @param high The highest possible number to choose from (not including this number)
	 * @return Returns the random number chosen BETWEEN low and high
	 */
	public int range(int low,int high)
	{
		Random r = new Random();
		return r.nextInt(high - (low + 1)) + (low + 1);
	}
	
	/**
	 * Returns the sum of all the numbers entered
	 * @param num A number
	 * @return The sum of all the numbers
	 */
	public int sum(int... num)
	{
		int val = 0;
		
		for(int i = 0; i < num.length; i++)
		{
			val += num[i];
		}
		
		return val;
	}
	
	/**
	 * Returns true or false depending if the string contains only numbers
	 * @param s The string to be tested
	 * @return Returns true if the entire string contains only numbers, otherwise it returns false
	 */
	public boolean isNumeric(String s)
	{
		return s != null && s.matches("[-+]?\\d*\\.?\\d+");
	}
	
	/**
	 * Returns an array list populated by all the characters from a string input
	 * @param s The string to be split
	 * @return Returns an array list of characters
	 */
	public ArrayList<Character> stringToCharArraylist(String s)
	{
		char[] c = s.toCharArray();
		ArrayList<Character> alc = new ArrayList<Character>();

		for(int i = 0; i < c.length; i++)
		{
			alc.add(c[i]);
		}
		
		return alc;
	}
	
	/**
	 * Returns an integer array list that has been sorted using a type of insertion sort
	 * @param alInt The array list
	 * @return The sorted version of the array list
	 */
	public ArrayList<Integer> insertSort(ArrayList<Integer> alInt)
	{
		ArrayList<Integer> sort = new ArrayList<Integer>();
		ArrayList<Integer> ali = new ArrayList<Integer>();
		
		ali = alInt;
		
		// While the unsorted array is still full of numbers, sort them
		while(ali.size() > 0)
		{
			// Get the first value of the array
			int num = ali.get(0);
			int pos = 0;
			
			// As long as there are at least 2 or more values in the array...
			if(ali.size() != 1)
			{
				// Compare it with every other number in the array, if a bigger one is found set it to the new bigger number
				for(int i = 0; i < ali.size(); i++)
				{
					if(num <= ali.get(i))
					{
						num = ali.get(i);
						pos = i;
					}
				}
			}
			
			// Add the cherry picked number to the new sorted array
			ali.remove(pos);
			sort.add(num);
		}
		
		return sort;
	}
	
	/**
	 * Returns a byte with new bits
	 * @param b The byte to manipulate
	 * @param bits The 8 bits to be set into the byte
	 * @return The byte with new set bits
	 */
	public byte setBits(byte b,String bits)
	{
		for(int i = 0; i < 8; i++)
		{
			if(bits.charAt(i) == '1')
			{
				b |= (1 << i);				
			}
			else
			{
				b &= ~(1 << i);
			}
		}
		
		return b;
	}
	
	public byte getBit(byte b,int pos)
	{
		return (byte) ((b >> pos) & 1);
	}
}