package com.laeven.api;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Scanner;
/**
* <h1>L Encoder</h1>
* The L Encoder program implements an application that
* encodes numbers as letters to take up less space in
* text files.
* To do this, you must generate a key using the <b>generateKey()</b>
* method.
* By default this will generate a random Alphabetical key by randomising
* upper and lower case characters from a-z to create a base 52 key
* <p>
*
* @author  Laeven
* @version 1.2
* @since   2018-08-25
*/
public class LEncoder
{
	String fileLine;
	Scanner file;
	ArrayList<Character> symbKey = new ArrayList<Character>();
	
	/**
	 * This method adds 2 strings together
	 * @param a String of number 1
	 * @param b String of number 2
	 * @return The new string
	 */
	public String add(String a,String b)
	{
		int val = decode(a) + decode(b);
		return encode(val);
	}
	
	/**
	 * This method takes an 64 bit string and decodes it back into its original form using the key
	 * @param character The encoded number
	 * @return The decoded number
	 */
	public long decode64(String character)
	{
		/**
		 * val - holds the number to be returned
		 * symbLen - the length of the encoded number
		 * power - the powered up base number
		 */
		long val = 0;
		int symbLen = character.length() - 1;
		long power = 0;
		
		if(!symbKey.isEmpty())
		{
			// If string size is bigger than 1 then times the number buy the base (size of the array)
			if(character.length() > 1)
			{
				for(int i = 0; i < character.length() - 1; i++)
				{
					/**
					 * Depending on how long the encoded number is, every extra character in the string after 1 must me multiplied by the base^x, x = symbLen
					 * Example: the encoded number "dcba" has 4 letter. Counting from right to left,
					 * a is added on to val
					 * b is equal to its place in the array * by the base ^ 1
					 * c is equal to its place in the array * by the base ^ 2
					 * d is equal to its place in the array * by the base ^ 3
					 * etc.
					 * base is the size of 'symbKey' as it holds every character, in other words the base is the total number of characters you use to encode with
					*/
					power = (long) Math.pow(symbKey.size(),symbLen);
					
					val += power * getValue(character.charAt(i));
					//System.out.println((symbKey.size() * symbLen) + "(" + symbLen + ")" + " " + getValue(character.charAt(i)));
					symbLen --;
				}
			}
			
			/**
			 * Add on the last value in the string
			 */
			val += getValue(character.charAt(character.length() - 1));
		}
		else
		{
			System.out.println("LEncoder>> No key has been loaded!");
		}
		
		return val;
	}
	
	/**
	 * This method takes a 64 bit number and encodes it using the key loaded
	 * @param num The number to be encoded
	 * @return The new encoded number
	 */
	public String encode64(long num)
	{
		String code = "";		// The encoded number
		long higherVal = 0;		// The number that is the total number of divisions of base
		int mod = 0;			// Cannot directly get a char from array using a long, so an INT must be used
		ArrayList<Character> encodedLetter = new ArrayList<Character>();
		
		if(!symbKey.isEmpty())
		{					
			/**
			 * Do a division and modules operator on the number to see how many times it will divide into the base
			 * Take the modulus number, encode it, and add it to the array
			 */
			mod = (int) (num % symbKey.size());
			encodedLetter.add(symbKey.get(mod));
			higherVal = num / symbKey.size();
			
			/**
			 *  If the divided number is bigger than the base we must divide it further, and keep doing it until it's less than the base
			 *  For every loop we add the remainder (modules) to the array after encoding it
			 */
			if(higherVal != 0)	// If the number got from the first operation is less than the base, we don't need to add another letter to it
			{
				while(higherVal > symbKey.size())
				{
					mod = (int) (higherVal % symbKey.size());
					encodedLetter.add(symbKey.get(mod));
					higherVal = higherVal / symbKey.size();
				}
				
				// If the number left over from division is equal to the base (equivalent of 9 -> 10), then we add a new character on the end
				if(higherVal == symbKey.size())
				{
					// Add a new letter
					encodedLetter.add(symbKey.get(0));
					encodedLetter.add(symbKey.get(1));
				}
				else
				{
					// Carry on adding letters as usual
					// Force higherVal to be an integer as the remaining number must be less than the base
					encodedLetter.add(symbKey.get((int) higherVal));
				}
			}
			
			/**
			 * Now that we have all the encoded letters they are all backwards! So we use a reverse loop to assemble them
			 * correctly into a string ready to be returned!
			 */
			for(int i = encodedLetter.size() - 1; i >= 0; i--)
			{
				code = code + String.valueOf(encodedLetter.get(i));
				//System.out.println("SIZE " + code + " " + i + " " + encodedLetter.size());
			}
		}
		else
		{
			System.out.println("LEncoder>> No key has been loaded!");
		}
		
		return code;
	}
	
	/**
	 * This method takes an 32 bit string and decodes it back into its original form using the key
	 * @param character The encoded number
	 * @return The decoded number
	 */
	public int decode(String character)
	{
		/**
		 * val - holds the number to be returned
		 * symbLen - the length of the encoded number
		 * power - the powered up base number
		 */
		int val = 0;
		int symbLen = character.length() - 1;
		int power = 0;
		
		if(!symbKey.isEmpty())
		{
			// If string size is bigger than 1 then times the number buy the base (size of the array)
			if(character.length() > 1)
			{
				for(int i = 0; i < character.length() - 1; i++)
				{
					/**
					 * Depending on how long the encoded number is, every extra character in the string after 1 must me multiplied by the base^x, x = symbLen
					 * Example: the encoded number "dcba" has 4 letter. Counting from right to left,
					 * a is added on to val
					 * b is equal to its place in the array * by the base ^ 1
					 * c is equal to its place in the array * by the base ^ 2
					 * d is equal to its place in the array * by the base ^ 3
					 * etc.
					 * base is the size of 'symbKey' as it holds every character, in other words the base is the total number of characters you use to encode with
					*/
					power = (int) Math.pow(symbKey.size(),symbLen);
					
					val += power * getValue(character.charAt(i));
					//System.out.println((symbKey.size() * symbLen) + "(" + symbLen + ")" + " " + getValue(character.charAt(i)));
					symbLen --;
				}
			}
			
			/**
			 * Add on the last value in the string
			 */
			val += getValue(character.charAt(character.length() - 1));
		}
		else
		{
			System.out.println("LEncoder>> No key has been loaded!");
		}
		
		return val;
	}
	
	/**
	 * This method takes a 32 bit number and encodes it using the key loaded
	 * @param num The number to be encoded
	 * @return The new encoded number
	 */
	public String encode(int num)
	{
		String code = "";		// The encoded number
		int higherVal = 0;		// The number that is the total number of divisions of base
		ArrayList<Character> encodedLetter = new ArrayList<Character>();
		
		if(!symbKey.isEmpty())
		{					
			/**
			 * Do a division and modules operator on the number to see how many times it will divide into the base
			 * Take the modulus number, encode it, and add it to the array
			 */
			encodedLetter.add(symbKey.get(num % symbKey.size()));
			higherVal = num / symbKey.size();
			
			/**
			 *  If the divided number is bigger than the base we must divide it further, and keep doing it until it's less than the base
			 *  For every loop we add the remainder (modules) to the array after encoding it
			 */
			if(higherVal != 0)	// If the number got from the first operation is less than the base, we don't need to add another letter to it
			{
				while(higherVal > symbKey.size())
				{
					encodedLetter.add(symbKey.get(higherVal % symbKey.size()));
					higherVal = higherVal / symbKey.size();
				}
				
				// If the number left over from division is equal to the base (equivalent of 9 -> 10), then we add a new character on the end
				if(higherVal == symbKey.size())
				{
					// Add a new letter
					encodedLetter.add(symbKey.get(0));
					encodedLetter.add(symbKey.get(1));
				}
				else
				{
					// Carry on adding letters as usual
					encodedLetter.add(symbKey.get(higherVal));
				}
			}			
			/**
			 * Now that we have all the encoded letters they are all backwards! So we use a reverse loop to assemble them
			 * correctly into a string ready to be returned!
			 */
			for(int i = encodedLetter.size() - 1; i >= 0; i--)
			{
				code = code + String.valueOf(encodedLetter.get(i));
				//System.out.println("SIZE " + code + " " + i + " " + encodedLetter.size());
			}
		}
		else
		{
			System.out.println("LEncoder>> No key has been loaded!");
		}
		
		return code;
	}
	
	/**
	 * This method returns the decoded number of an encoded number based on the key being used
	 * @param a The encoded number
	 * @return The decoded number
	 */
	public int getValue(char a)
	{
		int val = -1;	// 
		
		/**
		 *  Loops through the key array until the letter injected matches one in the key array
		 *  It will grab the counter as its number
		 */
		charcheck:
		for(int i = 0; i < symbKey.size(); i++)
		{
			if(a == symbKey.get(i))
			{
				val = i;
				break charcheck;
			}
		}
		
		return val;
	}
	
	/**
	 * This method returns the encoded number of a decoded number based on the key being used
	 * @param i The decoded number
	 * @return The encoded number
	 */
	public char getcharacter(int i)
	{
		char c = '!';
		
		try
		{
			c = symbKey.get(i);
		}
		catch(ArrayIndexOutOfBoundsException e)
		{
			System.out.println("LEncoder>> Error! character does not exist");
			e.printStackTrace();
		}
		
		return c;
	}
	
	/**
	 * This method generates a base 78 alphabetical and character key by randomising all upper and lower case letters with some additional characters
	 * <b>Note:</b> This method should only be called once to generate the initial key
	 * @param fileName The file name of the key
	 * @param randomised If the key should be randomised or not
	 */
	public void generateKey(String fileName,boolean randomised)
	{
		File f = new File(fileName);
		
		// Check if a key has already been generated
		if(f.exists() && !f.isDirectory())
		{
			System.out.println("LEncoder>> Key already exists! To generate a new one, delete the key!");
		}
		else
		{
			ArrayList<Character> symb = new ArrayList<Character>();
			LBasic basicRan = new LBasic();
			String s = "!#$%&()*+'-./^=`@:;~><[]{}";
			char[] extra = s.toCharArray();
			
			// Fill up the array with all 52 letters
			for(char letter = 'a'; letter <= 'z'; letter++)
			{
				symb.add(letter);
			}
			
			for(char letter = 'A'; letter <= 'Z'; letter++)
			{
				symb.add(letter);
			}
			
			// Add some other characters
			for(int i = 0; i < extra.length; i++)
			{
				symb.add(extra[i]);
			}
			
			// Randomly pick letters in the array and place them in a new array to create the key
			if(randomised)
			{
				symbKey.clear();
				
				while(symb.size() > 0)
				{
					int pos = basicRan.range(-1,symb.size());
					symbKey.add(symb.get(pos));
					symb.remove(pos);
				}
			}
			else
			{
				for(int i = 0; i < symb.size(); i++)
				{
					symbKey.add(symb.get(i));
				}
			}
			
			// Save the key as a text file
			PrintWriter writer = null;
			
			try
			{
				writer = new PrintWriter(fileName, "UTF-8");
				
				for(int i = 0; i < symbKey.size(); i++)
				{
					writer.println(symbKey.get(i));
				}
			}
			catch (FileNotFoundException e)
			{
				System.out.println("LEncoder>> Error! File not found!");
				e.printStackTrace();
			}
			catch (UnsupportedEncodingException e)
			{
				System.out.println("LEncoder>> Error! Unsupported encoding!");
				e.printStackTrace();
			}
			
			writer.close();
		}
	}
	
	/**
	 * This method generates a key by using custom characters from a specified text file
	 * <b>Note:</b> This method should only be called once to generate the initial key
	 * @param charFileName The file name containing the characters
	 * @param fileName The file name of the key
	 * @param randomised If the key should be randomised or not
	 */
	public void generateKeyCustom(String charFileName,String fileName,boolean randomised)
	{
		File f = new File(fileName);
		
		// Check if a key has already been generated
		if(f.exists() && !f.isDirectory())
		{
			System.out.println("LEncoder>> Key already exists! To generate a new one, delete the key!");
		}
		else
		{
		
			ArrayList<Character> symb = new ArrayList<Character>();
			String s = "";
			LBasic basicRan = new LBasic();
			
			try
			{
				file = new Scanner(new File(charFileName));
				
				// Load in file with characters and remove all the spaces to have 1 string of characters
				while(file.hasNextLine())
				{
					fileLine = file.nextLine();
					s = s + fileLine.replaceAll("\\s+","");
				}
				
				System.out.println(s);
				
				// If the custom amount of characters provided is smaller than 11, you might as well use regular numbers
				if(s.length() > 10)
				{
					// Convert the string to an array of chars and populate arraylist
					LBasic b = new LBasic();
					symb = b.stringToCharArraylist(s);
					
					// Randomly pick letters in the array and place them in a new array to create the key
					if(randomised)
					{
						symbKey.clear();
						
						while(symb.size() > 0)
						{
							int pos = basicRan.range(-1,symb.size());
							symbKey.add(symb.get(pos));
							symb.remove(pos);
						}
					}
					else
					{
						for(int i = 0; i < symb.size(); i++)
						{
							symbKey.add(symb.get(i));
						}
					}
					
					// Save the key as a text file
					PrintWriter writer = null;
					
					try
					{
						writer = new PrintWriter(fileName, "UTF-8");
						
						for(int i = 0; i < symbKey.size(); i++)
						{
							writer.println(symbKey.get(i));
						}
					}
					catch (FileNotFoundException e)
					{
						System.out.println("LEncoder>> Error! File not found!");
						e.printStackTrace();
					}
					catch (UnsupportedEncodingException e)
					{
						System.out.println("LEncoder>> Error! Unsupported encoding!");
						e.printStackTrace();
					}
					
					writer.close();
				}
				else
				{
					System.out.println("LEncoder>> Key failed to build: Must use more than 10 characters!");
				}
			
			}
			catch (FileNotFoundException e)
			{
				System.out.println("LEncoder>> Error! File not found!");
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * This method loads a pre-existing key file into memory. It is used to encode and decode numbers
	 * @param fileName The directory and/or name of the file
	 */
	public void loadKey(String fileName)
	{
		try
		{
			file = new Scanner(new File(fileName));
			
			symbKey.clear();
			
			while(file.hasNextLine())
			{
				fileLine = file.nextLine();
				symbKey.add(fileLine.charAt(0));
			}
		}
		catch (FileNotFoundException e)
		{
			System.out.println("LEncoder>> Error! File not found!");
			e.printStackTrace();
		}
	}
}
