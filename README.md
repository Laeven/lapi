# LAPI
An API I coded containing some tools

## Classes

Class | Status | Version | Date Init
--- | --- | --- | ---
LBasic | `Finished` | 1.1 | 2018-08-25
LEncoder | `Finished` | 1.2 | 2018-08-25
LCSV | `Finished` | 1.0 | 2018-08-25
LCompress| `In Progress` | 1.1 | 2018-08-25
LGUI | `In Progress` | 1.2 | 2018-09-06
LAction | `Finished` | 1.2 | 2018-09-06